package com.rudyr.research.soap.io;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RdySoapResearchIoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RdySoapResearchIoApplication.class, args);
	}

}
